//
//  ImageViewController.swift
//  Collector
//
//  Created by Cohen, Dor on 23/09/2020.
//

import UIKit

class ImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePicker = UIImagePickerController()

    @IBOutlet weak var collectorImage: UIImageView!
    @IBOutlet weak var collectorTitle: UITextField!
    
    @IBAction func collectorSave(_ sender: Any) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
            
        let item =  Item(entity: Item.entity(), insertInto: managedContext)

        item.title = collectorTitle.text!
        if let image = collectorImage.image{
            item.image = image.jpegData(compressionQuality: 1.0)
        }
        item.creationDate = Date()
        
        try? managedContext.save()
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func cameraAdd(_ sender: Any) {
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    @IBAction func imageAdd(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[.originalImage] as? UIImage{
            collectorImage.image = selectedImage
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.hideKeyboardWhenTappedAround() 
    }

}
